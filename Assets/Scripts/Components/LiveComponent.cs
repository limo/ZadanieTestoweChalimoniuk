using Unity.Entities;

public struct LiveComponent : IComponentData
{
    public int Lives;
    public int Id;
    public float TimeForNextSpawn;
}
