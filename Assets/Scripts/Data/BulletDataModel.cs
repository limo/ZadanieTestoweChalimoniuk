using Unity.Mathematics;

public struct BulletDataModel
{
    public float3 Direction;
    public float3 Position;
    public float LiveTime;
    public int CollideWithIndex;
    public int ParentID;
}
