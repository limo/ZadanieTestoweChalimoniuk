using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameParams", menuName = "Data/GameParams")]
public class GameParams : ScriptableObject
{
    public float ObjectSpacing = 2;
    public float BulletLiveTime = 10;
    public float TimeBetweenShots = 1;
    public float ObjectDeadTime = 1;
    public int LivesForObject = 3;
    public Mesh ObjectMesh;
    public Material ObjectMaterial;
    public Mesh BulletMesh;
    public Material BulletMaterial;
}
