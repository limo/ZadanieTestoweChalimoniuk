using UnityEngine;

[CreateAssetMenu(fileName = "ButtonsConfig", menuName = "Data/UI/ButtonsConfig")]
public class LevelButtonConfig : ScriptableObject
{
    public Sprite SelectedSprite;
    public Sprite UnselectedSprite;
    public Vector3 SelectedScale;
    public Vector3 UnselectedScale;
}
