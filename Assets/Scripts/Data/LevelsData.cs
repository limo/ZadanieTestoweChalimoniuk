using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class LevelDM
{
    public int ObjectCount;
}

[CreateAssetMenu(fileName ="LevelsData", menuName ="Data/LevelsData")]
public class LevelsData:ScriptableObject
{
    public LevelDM[] Levels;
}
