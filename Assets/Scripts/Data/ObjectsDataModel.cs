using Unity.Transforms;

public struct ObjectsDataModel
{
    public LocalToWorld Transform;
    public LiveComponent LiveData;
}
