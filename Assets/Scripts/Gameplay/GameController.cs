using System;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using UnityEngine.Rendering;

[BurstCompile]
public partial class GameController : MonoBehaviour
{
    [SerializeField] GameParams _params;

    private GridSystem _gridSystem;
    private float _timeToShoot = 3;
    private int _countOfObjectsThatLeft = 10;
    private int _size;
    private bool _bulletEntityWasCreated = false;
    private bool _gameOver = true;
    private NativeArray<Entity> _objectsEntities;
    NativeArray<ObjectsDataModel> _objectsDataModels;
    private Entity _bulletEntity;
    private EntityManager _entityManager;

    public Action OnGameOver;

    public void ChangeGameSize(int newSize)
    {
        _size = newSize;
    }

    public void StartGame()
    {
        _timeToShoot = _params.TimeBetweenShots;
        _countOfObjectsThatLeft = _size;

        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        _gridSystem = World.DefaultGameObjectInjectionWorld.GetExistingSystemManaged<GridSystem>();
        _gridSystem.SetSize(_size);
        if (!_bulletEntityWasCreated)
        {
            CreateBulletEntity();
        }

        Entity objectEntity = _entityManager.CreateEntity();
        {
            var desc = new RenderMeshDescription(
                shadowCastingMode: ShadowCastingMode.On,
                receiveShadows: true,
                renderingLayerMask: 1
            );
            var renderMeshArray = new RenderMeshArray(
                new Material[] { _params.ObjectMaterial },
                new Mesh[] { _params.ObjectMesh }
            );
            RenderMeshUtility.AddComponents(
                objectEntity, _entityManager,
                desc,
                renderMeshArray,
                MaterialMeshInfo.FromRenderMeshArrayIndices(0, 0)
            );
            _entityManager.AddComponentData(objectEntity, new RotationComponet { TimeForNextRotation = 0 });

            _entityManager.SetComponentData(objectEntity, new LocalToWorld { Value = float4x4.identity });
        }

        _objectsEntities = new(_size, Allocator.Persistent);
        _entityManager.Instantiate(objectEntity, _objectsEntities);
        _entityManager.DestroyEntity(objectEntity);

        float3[] entitieSpawnPositions = new float3[_size];

        for (int i = 0; i < _size; i++)
        {
            var position = _gridSystem.GetPositionOnGrid(_params.ObjectSpacing);
            _gridSystem.AddToGrid(new GridData { Id = i, Position = position });
            entitieSpawnPositions[i] = position;
            _entityManager.AddComponentData(_objectsEntities[i], new LiveComponent { Lives = 3, Id = i });
            _entityManager.SetComponentData(_objectsEntities[i], new LocalToWorld { Value = float4x4.Translate(position) });
        }

        var temp = _size / 2 + _params.ObjectSpacing;
        Camera.main.transform.position = new Vector3(temp, temp, -5);
        Camera.main.orthographicSize = temp + 3;
        _gameOver = false;
    }

    private void CreateBulletEntity()
    {
        _bulletEntity = _entityManager.CreateEntity();
        {
            var desc = new RenderMeshDescription(
                shadowCastingMode: ShadowCastingMode.On,
                receiveShadows: true,
                renderingLayerMask: 1
            );
            var renderMeshArray = new RenderMeshArray(
                new Material[] { _params.BulletMaterial },
                new Mesh[] { _params.BulletMesh }
            );
            RenderMeshUtility.AddComponents(
                _bulletEntity, _entityManager,
                desc,
                renderMeshArray,
                MaterialMeshInfo.FromRenderMeshArrayIndices(0, 0)
            );

            _entityManager.SetComponentData(_bulletEntity, new LocalToWorld
            {
                Value = float4x4.Translate(new float3(-100000, -100000, -10000))
            });
        }
        _bulletEntityWasCreated = true;
    }

    private void FixedUpdate()
    {
        if (CheckGameOver())
        {
            return;
        }

        UpdateObjectsData();
        ShootBullets();
        CheckCollision();

        _objectsDataModels.Dispose();
    }

    private void UpdateObjectsData()
    {
        _objectsDataModels = new NativeArray<ObjectsDataModel>(_objectsEntities.Length, Allocator.TempJob);
        EntityQuery entityQuery = _entityManager.CreateEntityQuery(typeof(LiveComponent), typeof(LocalToWorld));
        UpdateObjectDataJob updateObjectDataJob = new UpdateObjectDataJob
        {
            ObjectsDatas = _objectsDataModels,
            LiveData = entityQuery.ToComponentDataArray<LiveComponent>(Allocator.TempJob),
            Transforms = entityQuery.ToComponentDataArray<LocalToWorld>(Allocator.TempJob),
        };

        JobHandle updateObjectDataHandle = updateObjectDataJob.Schedule(_objectsEntities.Length, 10);
        updateObjectDataHandle.Complete();
        _objectsDataModels = updateObjectDataJob.ObjectsDatas;
        entityQuery.Dispose();
    }

    private struct UpdateObjectDataJob : IJobParallelFor
    {
        public NativeArray<ObjectsDataModel> ObjectsDatas;
        public NativeArray<LiveComponent> LiveData;
        public NativeArray<LocalToWorld> Transforms;

        public void Execute(int i)
        {
            ObjectsDatas[LiveData[i].Id] = new ObjectsDataModel { LiveData = LiveData[i], Transform = Transforms[i] };
        }
    }

    private void CheckCollision()
    {
        EntityQuery entityQuery = _entityManager.CreateEntityQuery(typeof(BulletComponent));
        NativeArray<BulletComponent> bulletComponents = entityQuery.ToComponentDataArray<BulletComponent>(Allocator.TempJob);

        if (bulletComponents.Length == 0)
        {
            bulletComponents.Dispose();
            return;
        }

        NativeArray<BulletDataModel> bulletsData = new NativeArray<BulletDataModel>(bulletComponents.Length, Allocator.TempJob);

        CollisionJob collisionJob = new CollisionJob
        {
            BulletsData = bulletsData,
            BulletComponents = bulletComponents,
            ObjectsToRotate = _objectsDataModels,
            GridMultiHashMap = _gridSystem.GridMultiHashMap,
        };

        JobHandle collisionJobHandle = collisionJob.Schedule(bulletComponents.Length, 10);
        collisionJobHandle.Complete();
        bulletsData = collisionJob.BulletsData;

        NativeArray<Entity> alliveBullets = entityQuery.ToEntityArray(Allocator.Temp);
        for (int i = 0; i < bulletsData.Length; i++)
        {
            if (bulletsData[i].CollideWithIndex != -1)
            {
                var entityIndex = bulletsData[i].CollideWithIndex;
                var liveData = _objectsDataModels[entityIndex].LiveData;
                var transform = _objectsDataModels[entityIndex].Transform;
                liveData.Lives--;
                liveData.TimeForNextSpawn = _params.ObjectDeadTime;
                _entityManager.SetComponentData(_objectsEntities[entityIndex], liveData);

                transform = new LocalToWorld { Value = float4x4.TRS(transform.Position, transform.Rotation, float3.zero) };

                _entityManager.SetComponentData(_objectsEntities[entityIndex], transform);
                if (liveData.Lives <= 0)
                {
                    _countOfObjectsThatLeft--;
                }
                _entityManager.DestroyEntity(alliveBullets[i]);
                continue;
            }

            if (bulletsData[i].LiveTime <= 0)
            {
                _entityManager.DestroyEntity(alliveBullets[i]);
                continue;
            }
        }

        alliveBullets.Dispose();
        entityQuery.Dispose();
        bulletsData.Dispose();
        bulletComponents.Dispose();
    }

    private void ShootBullets()
    {
        _timeToShoot -= Time.deltaTime;
        if (_timeToShoot <= 0 && _objectsEntities.Length > 0)
        {
            NativeArray<BulletDataModel> bulletDMs = new NativeArray<BulletDataModel>(_objectsEntities.Length, Allocator.TempJob);

            ShootJob shootJob = new ShootJob
            {
                ObjectsDataModel = _objectsDataModels,
                BulletDMs = bulletDMs,
                BulletLiveTime = _params.BulletLiveTime,
            };

            JobHandle shootJobHandle = shootJob.Schedule(_objectsEntities.Length, 10);
            shootJobHandle.Complete();

            bulletDMs = shootJob.BulletDMs;
            List<BulletDataModel> bulletsToCreate = new List<BulletDataModel>();

            for (int i = 0; i < bulletDMs.Length; i++)
            {
                if (bulletDMs[i].LiveTime == -1) continue;
                bulletsToCreate.Add(bulletDMs[i]);
            }

            var newBulletEntitys = new NativeArray<Entity>(bulletsToCreate.Count, Allocator.Temp);
            _entityManager.Instantiate(_bulletEntity, newBulletEntitys);

            for (int i = 0; i < bulletsToCreate.Count; i++)
            {
                _entityManager.AddComponentData(newBulletEntitys[i], new BulletComponent
                {
                    Direction = bulletsToCreate[i].Direction,
                    LiveTime = _params.BulletLiveTime,
                    ParentId = bulletsToCreate[i].ParentID,
                    Position = bulletsToCreate[i].Position
                });
                _entityManager.SetComponentData(newBulletEntitys[i], new LocalToWorld { Value = float4x4.TRS(bulletsToCreate[i].Position, quaternion.identity, new float3(0.3f, 0.3f, 0.3f)) });
            }

            _timeToShoot = _params.TimeBetweenShots;

            newBulletEntitys.Dispose();
            bulletDMs.Dispose();
        }
    }

    private bool CheckGameOver()
    {
        if (_countOfObjectsThatLeft <= 1 && !_gameOver)
        {
            foreach (var item in _objectsEntities)
            {
                _entityManager.DestroyEntity(item);
            }
            EntityQuery entityQuery = _entityManager.CreateEntityQuery(typeof(BulletComponent));
            var bullets = entityQuery.ToEntityArray(Allocator.Temp);
            foreach (var item in bullets)
            {
                _entityManager.DestroyEntity(item);
            }

            OnGameOver?.Invoke();
            _gameOver = true;
            entityQuery.Dispose();
            bullets.Dispose();
            return true;
        }
        return _gameOver;
    }

    [BurstCompile]
    private struct ShootJob : IJobParallelFor
    {
        [ReadOnly] public NativeArray<ObjectsDataModel> ObjectsDataModel;
        public NativeArray<BulletDataModel> BulletDMs;
        public float BulletLiveTime;

        public void Execute(int i)
        {
            BulletDataModel bulletData = new BulletDataModel();

            if (ObjectsDataModel[i].LiveData.Lives <= 0 || ObjectsDataModel[i].LiveData.TimeForNextSpawn > 0)
            {
                bulletData.LiveTime = -1;
            }
            else
            {
                var direction = ObjectsDataModel[i].Transform.Up;
                bulletData.Position = ObjectsDataModel[i].Transform.Position + direction;
                bulletData.Direction = direction;
                bulletData.ParentID = i;
            }

            BulletDMs[i] = bulletData;
        }
    }

    [BurstCompile]
    private struct CollisionJob : IJobParallelFor
    {
        [ReadOnly] public NativeArray<ObjectsDataModel> ObjectsToRotate;
        public NativeArray<BulletComponent> BulletComponents;
        public NativeArray<BulletDataModel> BulletsData;
        [ReadOnly] public NativeParallelMultiHashMap<int, GridData> GridMultiHashMap;

        public void Execute(int i)
        {

            BulletsData[i] = new BulletDataModel
            {
                Position = BulletComponents[i].Position,
                LiveTime = BulletComponents[i].LiveTime,
                ParentID = BulletComponents[i].ParentId,
            };

            int hashGridKey = GridSystem.GetPositionHashMapKey(BulletsData[i].Position);
            var collideWithIndex = FindeCollision(i, hashGridKey);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey + 1);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey - 1);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey + GridSystem.GridYMultiplayer);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey - GridSystem.GridYMultiplayer);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey - 1 - GridSystem.GridYMultiplayer);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey + 1 - GridSystem.GridYMultiplayer);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey - 1 + GridSystem.GridYMultiplayer);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }
            collideWithIndex = FindeCollision(i, hashGridKey - 1 + GridSystem.GridYMultiplayer);

            if (collideWithIndex != -1) { HideBullet(i, collideWithIndex); return; }

            var newBulletData = BulletsData[i];
            newBulletData.CollideWithIndex = -1;
            BulletsData[i] = newBulletData;
        }

        private void HideBullet(int index, int collideWithIndex)
        {
            var newBulletData = BulletsData[index];
            newBulletData.LiveTime = -1;
            newBulletData.CollideWithIndex = collideWithIndex;
            BulletsData[index] = newBulletData;
        }

        [BurstCompile]
        int FindeCollision(int bulletId, int hashGridKey)
        {
            GridData objectId;
            NativeParallelMultiHashMapIterator<int> gridMultiHasMapInterior;
            if (GridMultiHashMap.TryGetFirstValue(hashGridKey, out objectId, out gridMultiHasMapInterior))
            {
                do
                {
                    if (objectId.Id == BulletsData[bulletId].ParentID) continue;
                    if (ObjectsToRotate[objectId.Id].LiveData.Lives <= 0) continue;
                    var dist = Vector3.Distance(BulletsData[bulletId].Position, ObjectsToRotate[objectId.Id].Transform.Position);
                    if (dist < .6f)
                    {
                        return objectId.Id;
                    }
                }
                while (GridMultiHashMap.TryGetNextValue(out objectId, ref gridMultiHasMapInterior));
            }
            return -1;
        }
    }

    private void OnDestroy()
    {
        _objectsEntities.Dispose();
    }
}
