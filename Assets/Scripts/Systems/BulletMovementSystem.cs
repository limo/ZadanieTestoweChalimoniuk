using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public struct BulletComponent : IComponentData
{
    public float3 Direction;
    public float LiveTime;
    public int ParentId;
    public float3 Position;
}

public partial class BulletMovementSystem : SystemBase
{
    private static float3 GetScale(float4x4 matrix) => new float3(math.length(matrix.c0.xyz), math.length(matrix.c1.xyz), math.length(matrix.c2.xyz));

    protected override void OnUpdate()
    {
        var deltaTime = World.Time.DeltaTime;
        Entities.WithAll<BulletComponent>().ForEach((ref LocalToWorld localToWorld, ref BulletComponent bulletComponent) =>
        {
            if (bulletComponent.LiveTime > 0)
            {
                bulletComponent.LiveTime -= deltaTime;
                var position = localToWorld.Position;
                var rotation = localToWorld.Rotation;
                var scale = GetScale(localToWorld.Value);
                position += bulletComponent.Direction * deltaTime;
                bulletComponent.Position = position;
                var temp = float4x4.TRS(position, rotation, scale);
                localToWorld.Value = temp;
            }
        }).Schedule();
    }
}
