using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

public struct GridData
{
    public int Id;
    public float3 Position;
}

public partial class GridSystem : SystemBase
{
    static int _size;
    static float _spacing;
    static Random _random = new Random(256);
    private static NativeParallelMultiHashMap<int, GridData> _gridMultiHashMap;

    public const int GridCellSize = 3;
    public readonly static int GridYMultiplayer = 1000;
    public NativeParallelMultiHashMap<int, GridData> GridMultiHashMap { get { return _gridMultiHashMap; } set { _gridMultiHashMap = value; } }

    public static int GetPositionHashMapKey(float3 position)
    {
        return (int)(math.floor((position.x / GridCellSize) + (GridYMultiplayer * math.floor(position.y / GridCellSize))));
    }

    public void SetSize(int size)
    {
        _size = size;
        _gridMultiHashMap = new NativeParallelMultiHashMap<int, GridData>(size, Allocator.Persistent);
    }

    public void AddToGrid(GridData value)
    {
        _gridMultiHashMap.Add(GetPositionHashMapKey(value.Position), value);
    }

    public float3 GetPositionOnGrid(float spacing)
    {
        _spacing = spacing;
        return GetPositionOnGrid();
    }

    public static float3 GetPositionOnGrid(NativeParallelMultiHashMap<int, GridData> gridMultiHashMap= new NativeParallelMultiHashMap<int, GridData>())
    {
        if (!gridMultiHashMap.IsCreated)
        {
            gridMultiHashMap = _gridMultiHashMap;
        }
        float3 toReturn = new float3();
        float x = _random.NextFloat(-1f, _size * _spacing);
        float y = _random.NextFloat(-1f, _size * _spacing);

        int hashGridKey = GetPositionHashMapKey(toReturn);

        if (FindeCollisionOnGrid(toReturn, hashGridKey, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey + 1, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey - 1, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey + GridYMultiplayer, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey - GridYMultiplayer, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey + GridYMultiplayer + 1, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey + GridYMultiplayer - 1, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey - GridYMultiplayer + 1, gridMultiHashMap)) return GetPositionOnGrid();
        if (FindeCollisionOnGrid(toReturn, hashGridKey - GridYMultiplayer - 1, gridMultiHashMap)) return GetPositionOnGrid();

        toReturn = new float3(x, y, 0);
        return toReturn;
    }

    private static bool FindeCollisionOnGrid(float3 position, int hashGridKey, NativeParallelMultiHashMap<int, GridData> gridMultiHashMap)
    {
        float3 toRturn = position;
        GridData objectId;
        NativeParallelMultiHashMapIterator<int> gridMultiHasMapInterior;
        if (gridMultiHashMap.TryGetFirstValue(hashGridKey, out objectId, out gridMultiHasMapInterior))
        {
            do
            {
                var dist = Vector3.Distance(objectId.Position, toRturn);
                if (dist < _spacing)
                {
                    return true;
                }
            }
            while (gridMultiHashMap.TryGetNextValue(out objectId, ref gridMultiHasMapInterior));
        }
        return false;
    }

    protected override void OnDestroy()
    {
        GridMultiHashMap.Dispose();
        base.OnDestroy();
    }

    protected override void OnUpdate()
    {
        EntityQuery entityQuery = GetEntityQuery(typeof(LiveComponent), typeof(LocalToWorld));
        int count = entityQuery.CalculateEntityCount();
        if (count <= 0) return;
        _gridMultiHashMap.Clear();
        if (_gridMultiHashMap.Capacity < count)
        {
            _gridMultiHashMap.Capacity = count;
        }

        new SetGridHashMapData { GridMultiHashMap= _gridMultiHashMap.AsParallelWriter()}.ScheduleParallel(entityQuery);
        new UpdateTimeOnObjects{ GridMultiHashMap = _gridMultiHashMap,DeltaTime = World.Time.DeltaTime }.Schedule(entityQuery);
    }

    [BurstCompile]
    private partial struct SetGridHashMapData : IJobEntity
    {
        public NativeParallelMultiHashMap<int, GridData>.ParallelWriter GridMultiHashMap;
        public void Execute(ref LiveComponent liveComponent, ref LocalToWorld localToWorld)
        {
            if (liveComponent.Lives <= 0) return;
            int hashMapKey = GetPositionHashMapKey(localToWorld.Position);
            if (liveComponent.TimeForNextSpawn <= 0)
            {
                var objetctValue = new GridData { Id = liveComponent.Id, Position = localToWorld.Position };

                GridMultiHashMap.Add(hashMapKey, objetctValue);
            }
        }
    }

    private partial struct UpdateTimeOnObjects: IJobEntity
    {
        public float DeltaTime;
        public NativeParallelMultiHashMap<int, GridData> GridMultiHashMap;
        public void Execute(ref LiveComponent liveComponent, ref LocalToWorld localToWorld)
        {
            if (liveComponent.Lives <= 0) return;
            //nowy job pod to
            if (liveComponent.TimeForNextSpawn > 0)
            {
                liveComponent.TimeForNextSpawn -= DeltaTime;
                if (liveComponent.TimeForNextSpawn <= 0)
                {
                    var position = GetPositionOnGrid(GridMultiHashMap);
                    var quaternion = localToWorld.Rotation;
                    var temp = float4x4.TRS(position, quaternion, new float3(1));
                    localToWorld.Value = temp;
                }
            }
        }
    }
}
