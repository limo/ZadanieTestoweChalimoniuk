using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

public partial class RotationSystem : SystemBase
{
    private static float3 GetScale(float4x4 matrix) => new float3(math.length(matrix.c0.xyz), math.length(matrix.c1.xyz), math.length(matrix.c2.xyz));

    protected override void OnUpdate()
    {
        var deltaTime = World.Time.DeltaTime;
        Random random = new Random();
        random.InitState((uint)(DateTime.Now.Ticks));

        Entities.WithAll<RotationComponet, LiveComponent>().ForEach((ref LocalToWorld localToWorld, ref LiveComponent liveComponent, ref RotationComponet rotationComponet) =>
              {
                  if (liveComponent.Lives > 0)
                  {
                      if (liveComponent.TimeForNextSpawn > 0)
                      {
                          return;
                      }
                      else if (rotationComponet.TimeForNextRotation <= 0)
                      {
                          var newRotationAngle = random.NextFloat(0f, 360f);
                          var newQuaternion = Quaternion.Euler(0, 0, newRotationAngle);
                          var newQuaternionStruct = new quaternion { value = new float4(newQuaternion.x, newQuaternion.y, newQuaternion.z, newQuaternion.w) };

                          var position = localToWorld.Position;
                          rotationComponet.TimeForNextRotation = random.NextFloat(0f, 1f);
                          var scale = GetScale(localToWorld.Value);
                          var temp = float4x4.TRS(position, newQuaternionStruct, scale);
                          localToWorld.Value = temp;
                      }
                      else
                      {
                          rotationComponet.TimeForNextRotation -= deltaTime;
                      }
                  }
              }).Schedule();
    }
}
