using System;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{

    public int GameplaySize;
    private Button _button;
    private LevelButtonConfig _config;

    public void Init(LevelButtonConfig config, Action onButtonClicked)
    {
        _button = GetComponent<Button>();
        _config = config;
        _button.onClick.AddListener(()=>onButtonClicked.Invoke());
    }

    public void Select()
    {
        _button.image.sprite = _config.SelectedSprite;
        _button.transform.localScale = _config.SelectedScale;
    }

    public void Unselect()
    {
        _button.image.sprite = _config.UnselectedSprite;
        _button.transform.localScale = _config.UnselectedScale;
    }
}
