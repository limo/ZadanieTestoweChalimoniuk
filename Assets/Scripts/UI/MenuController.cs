using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private LevelButtonConfig _config;
    [SerializeField] private Button _startButton;
    [SerializeField] private GameObject _inactiveStartButton;
    [SerializeField] private GameObject _levelButtonHolder;
    [SerializeField] private GameController _gameController;
    [SerializeField] private GameObject _sizeSelectionPanel;
    [SerializeField] private GameObject _gameOverPanel;
    [SerializeField] private Button _restarGameButton;

    private LevelButton[] _levelsButtons;

    private bool _isStartButtonVisible;
    private bool _isReady;

    private void OnEnable()
    {
        Init();
        HideStartButton();
    }

    private void Init()
    {
        if (_isReady) return;
        _levelsButtons = _levelButtonHolder.GetComponentsInChildren<LevelButton>();
        for (int i = 0; i < _levelsButtons.Length; i++)
        {
            var itemID = i;

            _levelsButtons[itemID].Init(_config, () => SelectButton(itemID));
        }
        _startButton.onClick.AddListener(StartGamePlay);
        _restarGameButton.onClick.AddListener(RestartMenu);
        _gameController.OnGameOver = ShowGameOverScreen;
        RestartMenu();
        _isReady = true;
    }

    private void ShowStartButton()
    {
        if (_isStartButtonVisible) return;
        _startButton.gameObject.SetActive(true);
        _inactiveStartButton.SetActive(false);
        _isStartButtonVisible = true;
    }
    private void HideStartButton()
    {
        if (!_isStartButtonVisible) return;
        _startButton.gameObject.SetActive(false);
        _inactiveStartButton.SetActive(true);
        _isStartButtonVisible = false;
    }

    private void SelectButton(int id)
    {
        for (int i = 0; i < _levelsButtons.Length; i++)
        {
            if (i != id)
                _levelsButtons[i].Unselect();
            else
            {
                _levelsButtons[i].Select();
                _gameController.ChangeGameSize(_levelsButtons[i].GameplaySize);
            }
        }
        ShowStartButton();
    }

    private void StartGamePlay()
    {
        SelectButton(-1);
        HideStartButton();
        _gameController.StartGame();
        _sizeSelectionPanel.SetActive(false);
    }

    private void ShowGameOverScreen()
    {
        _gameOverPanel.SetActive(true);
    }

    private void RestartMenu()
    {
        _sizeSelectionPanel.SetActive(true);
        _gameOverPanel.SetActive(false);
    }
}
